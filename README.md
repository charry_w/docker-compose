# docker-compose





## Docker安装

[Ubuntu - Docker —— 从入门到实践 (gitbook.io)](https://yeasy.gitbook.io/docker_practice/install/ubuntu)

```shell
# 脚本下载地址
# https://get.daocloud.io/docker
# https://get.docker.com/

# Option1 下载并运行docker一键安装脚本
curl -sSL https://get.daocloud.io/docker | sh
# Option2 下载脚本 指定阿里源运行脚本
curl -fsSL get.docker.com -o get-docker.sh
sudo sh get-docker.sh --mirror Aliyun

# 启动docker服务
systemctl start docker
# wsl systemctl不可用，需用service
service docker start
# 查看docker版本
docker -v
```



## 建立 docker 用户组

> 默认情况下，`docker` 命令会使用 [Unix socket](https://en.wikipedia.org/wiki/Unix_domain_socket) 与 Docker 引擎通讯。而只有 `root` 用户和 `docker` 组的用户才可以访问 Docker 引擎的 Unix socket。出于安全考虑，一般 Linux 系统上不会直接使用 `root` 用户。因此，更好地做法是将需要使用 `docker` 的用户加入 `docker` 用户组。

```shell
# 建立 docker 组：
sudo groupadd docker
# 将当前用户加入 docker 组：
sudo usermod -aG docker $USER
```



## 配置镜像加速

> [镜像加速器 - Docker —— 从入门到实践 (gitbook.io)](https://yeasy.gitbook.io/docker_practice/install/mirror)

```shell
# 可以在 /etc/docker/daemon.json 中写入如下内容（如果文件不存在请新建该文件）：
{
  "registry-mirrors": [
    "https://hub-mirror.c.163.com",
    "https://mirror.baidubce.com"
  ]
}

# 注意，一定要保证该文件符合 json 规范，否则 Docker 将不能启动。
# 之后重新启动服务。
$ sudo systemctl daemon-reload
$ sudo systemctl restart docker
```



## Docker-compose安装

 ```shell
 # docker-compose脚本仓库
 # https://github.com/docker/compose/tags
 # 2.0版本安装参考 https://docs.docker.com/compose/cli-command/#install-on-linux
 
 # 2.0之前的版本 下载docker-compose运行脚本（已替换国内加速）
 curl -L https://download.fastgit.org/docker/compose/releases/download/1.29.2/run.sh > /usr/local/bin/docker-compose
 # 脚本文件添加可执行权限
 chmod +x /usr/local/bin/docker-compose
 # 查看docker-compose版本
 docker-compose --version
 ```



## Docker-compose 相关命令

> [命令说明 - Docker —— 从入门到实践 (gitbook.io)](https://yeasy.gitbook.io/docker_practice/compose/commands)
>
> [Compose 模板文件 - Docker —— 从入门到实践 (gitbook.io)](https://yeasy.gitbook.io/docker_practice/compose/compose_file)

```shell
# 构建镜像
docker-compose build
# 构建镜像，--no-cache表示不用缓存，否则在重新编辑Dockerfile后再build可能会直接使用缓存而导致新编辑内容不生效
docker-compose build --no-cache
# config 校验文件格式是否正确
docker-compose -f docker-compose.yml config
# 运行服务
ocker-compose up -d
# 运行服务 控制运行后的默认network、服务名
ocker-compose -p 服务名 up -d
# 启动/停止服务
docker-compose start/stop 服务名
# 停止服务
docker-compose down
# 停止服务 并同时删除调用的其他容器
docker-compose down --remove-orphans
# 查看容器日志
docker logs -f 容器ID
# 查看镜像
docker-compose images
# 拉取镜像
docker-compose pull 镜像名
```

## 常见 shell 组合

```shell
# 删除所有容器
docker stop `docker ps -q -a` | xargs docker rm
# 删除所有标签为none的镜像
docker images|grep \<none\>|awk '{print $3}'|xargs docker rmi
# 查找容器IP地址
docker inspect 容器名或ID | grep "IPAddress"
# 创建网段, 名称: mynet, 分配两个容器在同一网段中 (这样子才可以互相通信)
docker network create mynet
docker run -d --net mynet --name container1 my_image
docker run -it --net mynet --name container1 another_image
```

---

## 环境准备

```shell
# 创建公用网络，方便容器间调用
docker network create common-netwok
# 创建macvlan网络，使容器有独立ip
docker network create -d macvlan -o parent=eth0 --subnet=192.168.0.0/24 --gateway=192.168.0.1  macvlan_net

# 创建macvlan网络，是容器有独立pi，并配置默认ip段
docker network create \
-d macvlan \
-o parent=eth0 \
--subnet=192.168.50.0/24 \
--ip-range=192.168.50.33/28 \
--gateway=192.168.50.1 \
openwrt-macvlan-net

```

## 运行服务

### Portainer

```shell
docker volume create portainer_data
docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce

# 参数说明
--restart=always #Docker重启后该容器也为随之重启
-v /var/run/docker.sock:/var/run/docker.sock #容器中的进程可以通过它与Docker守护进程进行通信
-d #后台模式
--name portainer #容器命名为portainer


docker-compose -f docker-compose-portainer.yml up -d

docker-compose -f docker-compose-portainer.yml -p portainer up -d
-p：项目名称
-f：指定docker-compose.yml文件路径
-d：后台启动
```

访问地址：[`ip地址:9000`](http://127.0.0.1:9000/)

### MySQL

```shell
# 5.7
docker-compose -f docker-compose-mysql5.7.yml up -d
docker-compose -f docker-compose-mysql5.7.yml -p mysql5.7 up -d
```

### Redis

```shell
# 当前目录下所有文件赋予权限(读、写、执行)
chmod -R 777 ./redis
# 运行
docker-compose -f docker-compose-redis.yml -p redis up -d
docker-compose -f docker-compose-redis.yml up -d
```

### Nacos

```shell
# 需自己建库`nacos_config`, 并执行`/Liunx/nacos_mysql/nacos-mysql.sql`脚本
docker-compose -f docker-compose-nacos.yml up -d
```

访问地址：[`ip地址:8848/nacos`](http://127.0.0.1:8848/nacos/)

登录账号密码：`root/password`

### RabbitMQ

```shell
# 当前目录下所有文件赋予权限(读、写、执行)
chmod -R 777 ./rabbitmq
# 运行 [ 注：如果之前有安装过，需要清除浏览器缓存和删除rabbitmq相关的存储数据(如:这里映射到宿主机的data数据目录)，再重装，否则会出现一定问题！ ]
docker-compose -f docker-compose-rabbitmq.yml up -d
```

web管理端：[`ip地址:15672`](http://127.0.0.1:15672/)
登录账号密码：`guest/guest`

### MongoDB - 基于文档的通用分布式数据库

```shell
docker-compose -f docker-compose-mongodb.yml up -d
```

### MinIO - 高性能对象存储

```shell
docker-compose -f docker-compose-minio.yml up -d
```

### ElasticSearch

```shell
# 当前目录下所有文件赋予权限(读、写、执行)
chmod -R 777 ./elasticsearch
# 运行
docker-compose -f docker-compose-elasticsearch.yml -p elasticsearch up -d
docker-compose -f docker-compose-elasticsearch.yml up -d
```

kibana访问地址：[`ip地址:5601/app/dev_tools#/console`](http://127.0.0.1:5601/app/dev_tools#/console)

